
(defun kill-other-buffers ()
  "Kill all other buffers."
  (interactive)
  (mapc 'kill-buffer 
        (delq (current-buffer) 
              (remove-if-not 'buffer-file-name (buffer-list)))))

(defun sarguin-w80 ()
  (interactive)
  (setq mysize (- 80 (window-body-width)))
  (window-resize nil mysize t))

(defun sa-datetime (arg)
  "Default            : yyyy-mm-dd
C-u              : yyyy-mm-dd t
C-u C-u          : dd-mm-yyyy
C-u C-u C-u      : dd-mm-yyyy t"
  
  (interactive "P")
  (cond ((equal arg '(4)) (insert (format-time-string "%Y-%m-%d %T")))
        ((equal arg '(16)) (insert (format-time-string "%d-%m-%Y")))
        ((equal arg '(64)) (insert (format-time-string "%d-%m-%Y %T")))
        (t (insert (format-time-string "%Y-%m-%d")))))

(provide 'sarguin-utils)
